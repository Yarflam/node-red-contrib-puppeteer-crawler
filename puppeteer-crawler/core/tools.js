const TYPE_NUMBER = 'number';
const TYPE_STRING = 'string';
const TYPE_OBJECT = 'object';
const TYPE_ARRAY = 'array';
const TYPE_BOOLEAN = 'boolean';

const tools = {
    /* Node-RED functions */
    nodeRedDebug: (msg, { topic, node, RED }) => {
        /* NPM Source: @node-red/nodes/core/common/21-debug */
        RED.comms.publish('debug', {
            id: node.id,
            z: node.z,
            _alias: node._alias,
            path: node._flow.path,
            name: node.name,
            topic,
            msg
        });
    },
    /* Deep object */
    pNested: (obj, struct, empty = null) => {
        if (!tools.isString(struct)) return empty;
        let walk = obj;
        for (let name of struct.split('.')) {
            if (!tools.isObject(walk) || tools.isUndefined(walk[name])) {
                walk = empty;
                break;
            }
            /* Continue */
            walk = walk[name];
        }
        return walk;
    },
    pNestedSec: (obj, struct, type, empty) =>
        tools.secureData(tools.pNested(obj, struct, empty), type),
    /* Security types */
    secureData: (obj, type) => {
        if (type === TYPE_NUMBER) {
            obj = Number(obj);
            if (isNaN(obj)) obj = 0;
            return obj;
        }
        if (type === TYPE_STRING) return String(obj) || '';
        if (type === TYPE_OBJECT) return tools.isObject(obj) ? obj : {};
        if (type === TYPE_ARRAY) return tools.isArray(obj) ? obj : [];
        if (type === TYPE_BOOLEAN) return Boolean(obj);
        return null;
    },
    getType: obj => {
        if (tools.isNumber(obj)) return TYPE_NUMBER;
        if (tools.isString(obj)) return TYPE_STRING;
        if (tools.isObject(obj)) return TYPE_OBJECT;
        if (tools.isArray(obj)) return TYPE_ARRAY;
        if (tools.isBoolean(obj)) return TYPE_BOOLEAN;
        return null;
    },
    sameType: (nextObj, oldObj) =>
        tools.secureData(nextObj, tools.getType(oldObj)),
    /* Types */
    isNumber: obj => typeof obj === 'number' && !isNaN(obj),
    isString: obj => typeof obj === 'string',
    isObject: obj => typeof obj === 'object' && obj !== null,
    isArray: obj => Array.isArray(obj),
    isFunction: obj => typeof obj === 'function',
    isBoolean: obj => typeof obj === 'boolean',
    isUndefined: obj => typeof obj === 'undefined'
};

module.exports = tools;
