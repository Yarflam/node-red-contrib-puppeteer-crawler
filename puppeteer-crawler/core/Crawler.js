const ThreadManager = require('./ThreadManager');
const tools = require('./tools');

const Puppeteer = require('puppeteer');
const path = require('path');

class Crawler {
    constructor() {
        this._config = {
            /* General options */
            userAgent: 'PuppeteerCrawler',
            nbThreads: 1,
            waitDuration: 30,
            notVerifyTLS: false,
            ignoreDroppedRequests: true,
            verbose: false,
            /* Browser options */
            runScripts: true,
            loadCSS: true
        };
        this._threads = new ThreadManager(this._config.nbThreads);
        this._fctPipe = null;
        this._fctLogs = null;
    }

    request(msg) {
        /* Check the parameters */
        if (!tools.pNestedSec(msg, 'payload.url', 'string')) {
            this.__logs('[PTC] Error: wrong parameters');
            return;
        }
        /* Create a new thread */
        this._threads.create(thread => this.__runBrowser(thread, msg));
        return this;
    }

    async __runBrowser(thread, msg) {
        this.__logs(`[PTC] RUN #${thread.id}`);
        /* Create the browser */
        const browser = await Puppeteer.launch({
            headless: true,
            args: [
                '--no-zygote',
                '--no-sandbox',
                // '--disable-gpu',
                // '--lang=fr-FR,fr',
                '--single-process',
                '--disable-infobars',
                '--disable-geolocation',
                '--disable-notifications',
                '--disable-setuid-sandbox',
                '--disable-smooth-scrolling',
                '--enable-natural-scroll-default',
                '--silent-debugger-extension-api',
                '--disable-session-crashed-bubble',
                `--user-data-dir=${path.resolve(__dirname, '../../userdata')}`
                // `--window-size=${SCREEN.join(',')}`,
                // `--proxy-server=${proto}://${host}:${port}`,
            ]
        });
        const close = () => {
            this.__logs(`[PTC] CLOSE #${thread.id}`);
            browser.close();
            thread();
        };
        // ignoreDroppedRequests: this._config.ignoreDroppedRequests,
        // runScripts: this._config.runScripts,
        // loadCSS: this._config.loadCSS,
        /* Request */
        try {
            let page = await browser.newPage();
            await page.setUserAgent(this._config.userAgent);
            // await page.setViewport({
            //     width: Number(SCREEN[0]),
            //     height: Number(SCREEN[1])
            // });
            await page.setDefaultNavigationTimeout(
                this._config.waitDuration * 1000
            );
            await page.goto(msg.payload.url);
            this.__logs(`[PTC] URL ${msg.payload.url}`);
            /* Treatment */
            const scripts = async () => {
                const payload = `(function ${this.__evalBrowser.toString()})(window, document, [${tools
                    .pNestedSec(msg, 'payload.scripts', 'array')
                    .filter(fct => tools.isFunction(fct))
                    .map(fct => fct.toString())
                    .join(',')}])`;
                return await page.evaluate(payload);
            };
            /* Mode 1. Execute Scenario */
            const scenario = tools.pNested(msg, 'payload.scenario');
            if (tools.isFunction(scenario)) {
                scenario(
                    browser,
                    out => {
                        if (tools.isArray(out)) {
                            out.forEach(res => this.__pipe(msg, res));
                        } else this.__pipe(msg, out);
                        close();
                    },
                    { msg, scripts }
                );
                return;
            }
            /* Mode 2. Execute Scripts */
            (await scripts()).forEach(res => this.__pipe(msg, res));
            close();
        } catch (e) {
            this.__logs(`[PTC] Error: ${e}`);
            this.__pipe(msg, null, e);
        }
    }

    __evalBrowser(window, document, scripts) {
        /* No script -> return page content */
        if (!scripts.length) return [document.querySelector('html').innerHTML];
        /* Execute the scripts */
        let out = [];
        for (let script of scripts) out.push(script());
        return out;
    }

    __pipe(msg, data, errors) {
        if (!this._fctPipe) return;
        this._fctPipe(msg, data, errors);
    }

    __logs(msg) {
        if (!this._config.verbose || !this._fctLogs) return;
        if (!tools.isString(msg) || !msg.length) return;
        this._fctLogs(msg);
    }

    loadConfig(config) {
        for (let key of Object.keys(this._config)) {
            if (!config[key]) continue;
            this._config[key] = tools.sameType(config[key], this._config[key]);
        }
        /* Update the nb threads */
        this._threads.setLimit(this._config.nbThreads);
        return this;
    }

    pipe(fct) {
        if (!tools.isFunction(fct)) return;
        this._fctPipe = fct;
        return this;
    }

    logs(fct) {
        if (!tools.isFunction(fct)) return;
        this._fctLogs = fct;
        return this;
    }
}

module.exports = Crawler;
